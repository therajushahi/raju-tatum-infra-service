## Tatum cloud infra
IaC
Endpoint to serve data to http://data-s3-bucket-tatum.s3-website.eu-central-1.amazonaws.com

## Description
This project will recevie data calling api from https://openweathermap.org/api and store that in a json file, this file will be uploaded to S3 the data will be served and displayed via index.html file in the same bucket.

The projects tree looks like following:
```bash
├── README.md
├── .gitlab-ci.yaml
├── app
│   ├── Dockerfile
│   ├── app.py
│   ├── html
│   │   └── index.html
│   └── requirements.txt
└── terraform
    └── eks
        ├── README.md
        ├── ecr.tf
        ├── eks.tf
        ├── k8s_cron.tf
        ├── kubernetes.tf
        ├── main.tf
        ├── outputs.tf
        ├── policy
        │   └── s3-policy.json
        ├── provider.tf
        ├── s3.tf
        ├── variables.tf
        ├── versions.tf
        └── vpc.tf
```

This project will provision the following components via terraform to AWS:
- VPC
- Networking
- ECR
- S3
- EKS
- Build dockerfile and push to ECR repo
- Push index.html file to S3 Bucket
- Provision cronjob resource to kubernetes Cluster


## Installation
To work localy you will need this tools:
- Terraform
- AWS
- Docker
- Python
- Kubectl
- Kubectx (Optional)
- k9s (Optional)

## Usage
Connect to Gitlab remote backend to work with this project localy:
Follow this instruction - https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html


## Support
Mail me at mail@rajushahi.com if there is any questions related to the project.

## Roadmap
There is still some more work to be done in the project listed below:
- Secret handling for the api calls in app.py service.
- Handling roles and permissions more granularly.
-

## Troubles 
I came a cross some issues while working on this project stated below:
* While trying to create EKS localy with the module I changed the region and applied the change. This did not result in what I expected to happen. My teori was that it would destroy the previous cluster and vpc but It did not. Looking at the plan it did only destroy a couple of resources but not the entire VPC and EKS. And recreated the resources again resulting in multiple VPC.
* While running the build for docker on my mac running Silicon ARC it gave an error when deploying to kubernetes cluster. "kubernetes standard_init_linux.go:228: exec user process caused: exec solution" Solution was to use this parameter in build cmd --platform linux/amd64
* Specific role for ECR push, Trying to use path for roles on EKS nodes to push to ECR didn't work as the role is used to create and provision node that will join the kubernetes control plane. It got stucka nodes never joined due to using role path.
* has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. This error accures when trying to serve html via S3 to solve it we need to add in
cors_rule = [
    {
    allowed_headers = ["*"]
    allowed_methods = ["GET","PUT","POST","DELETE"]
    allowed_origins = ["*"]
    expose_headers  = ["x-amz-server-side-encryption", "x-amz-request-id", "x-amz-id-2"]
    max_age_seconds = 3000
     }
  ]


## Authors and acknowledgment
Raju Shahi

## Project status
Fully updated and working as of today date: 2022-04-26

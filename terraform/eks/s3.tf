################################################################################
# PROVIDER
################################################################################

module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "data-s3-bucket-${local.name}"
  acl    = "private"

  force_destroy = true

  # Bucket policies
  attach_policy = true
  policy        = file("./policy/s3-policy.json")

  cors_rule = [
    {
    allowed_headers = ["*"]
    allowed_methods = ["GET","PUT","POST","DELETE"]
    allowed_origins = ["*"]
    expose_headers  = ["x-amz-server-side-encryption", "x-amz-request-id", "x-amz-id-2"]
    max_age_seconds = 3000
     }
  ]

  website = {
    # conflicts with "error_document"
    #        redirect_all_requests_to = {
    #          host_name = "https://modules.tf"
    #        }

    index_document = "index.html"
  }


  versioning = {
    enabled = true
  }

}

# Upload an object
resource "aws_s3_object" "object" {
  bucket       = "data-s3-bucket-${local.name}"
  key          = "index.html"
  source       = "../../app/html/index.html"
  content_type = "text/html"

  depends_on = [
    module.s3_bucket
  ]
}
locals {
  name            = "tatum"
  cluster_version = "1.21"
  region          = "eu-central-1"

  tags = {
    Service    = local.name
    Costcenter = "tatum"
  }
}

variable "policie_arn" {
  type    = string
  default = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
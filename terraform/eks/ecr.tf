################################################################################
# ECR
################################################################################

module "ecr" {
  source = "terraform-aws-modules/ecr/aws"

  repository_name                 = "tatum-repo"
  repository_image_tag_mutability = "MUTABLE"

  repository_read_write_access_arns = ["arn:aws:iam::855766564971:role/aws-service-role/eks-nodegroup.amazonaws.com/AWSServiceRoleForAmazonEKSNodegroup"]
  repository_lifecycle_policy = jsonencode({
    rules = [
      {
        rulePriority = 1,
        description  = "Keep last 30 images",
        selection = {
          tagStatus     = "tagged",
          tagPrefixList = ["v"],
          countType     = "imageCountMoreThan",
          countNumber   = 30
        },
        action = {
          type = "expire"
        }
      }
    ]
  })

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "ecr_mirror" {
  source         = "TechToSpeech/ecr-mirror/aws"
  docker_source  = "../../app"
  aws_account_id = "855766564971"
  aws_region     = "eu-central-1"
  ecr_repo_name  = "tatum-repo"
  ecr_repo_tag   = "latest"

  depends_on = [
    module.ecr
  ]
}
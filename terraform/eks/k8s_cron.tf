resource "kubernetes_cron_job" "app" {
  metadata {
    name = "pyton-app"
  }
  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 5
    schedule                      = "*/5 * * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10
    job_template {
      metadata {}
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 10
        template {
          metadata {}
          spec {
            container {
              name              = "python-app"
              image             = "855766564971.dkr.ecr.eu-central-1.amazonaws.com/tatum-repo:latest"
              image_pull_policy = "Always"
            }
          }
        }
      }
    }
  }
  depends_on = [
    module.ecr
  ]
}
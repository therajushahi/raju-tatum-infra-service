import requests
import math
import json
import boto3
from pprint import pprint
import pathlib
import os
 
city_name = ["Uppsala, SE", "Sydney, AU", "Stockholm, SE", "Washington, US", "Dhaka, BD", "	Lisbon, PT", "Berlin, DE"]
api_key = "ad715fa6fadc10089124215d0636252e"
length = len(city_name)
data = []
#aws_access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
#aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')

def get_weather(api_key, city_name):
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={api_key}"

    response = requests.get(url).json()

    temp = response['main']['temp']
    temp = math.floor((temp * 1.8) - 459.67)  # Convert to °F

    feels_like = response['main']['feels_like']
    feels_like = math.floor((feels_like * 1.8) - 459.67)  # Convert to °F

    humidity = response['main']['humidity']
    
    return {

        'city': city_name,
        'temp': temp,
        'feels_like': feels_like,
        'humidity': humidity
    }

length = len(city_name)

for city in range(length):
    weather = get_weather(api_key, city_name[city])
    data.append(weather)

with open('data.json', 'w', encoding='utf-8') as f:
    json.dump(data, f, ensure_ascii=False, indent=4)


def upload_file_using_client():
    """
    Uploads file to S3 bucket using S3 client object
    :return: None
    """
    s3 = boto3.client("s3")
    #aws_access_key_id=aws_access_key_id,
    #aws_secret_access_key=aws_secret_access_key

    bucket_name = "data-s3-bucket-tatum"
    object_name = "data.json"
    file_name = os.path.join(pathlib.Path(__file__).parent.resolve(), "data.json")
 
    response = s3.upload_file(file_name, bucket_name, object_name, ExtraArgs={'ContentType': "application/json", 'ACL': "public-read"})
    print(file_name)

upload_file_using_client()